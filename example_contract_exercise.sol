pragma solidity ^0.4.19;


contract TokenContract{
    
    string public name;
    string public symbol;
    uint8 public decimals = 0;
    uint256 public totalSupply;

    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    /**
    * constructor
    */
    constructor(
        uint256 initialSupply,
        string tokenName,
        string tokenSymbol
    ) public {
        totalSupply = initialSupply * 10 ** uint256(decimals);      // Update total supply with the decimal amount
        balanceOf[msg.sender] = totalSupply;                        // Give the creator all initial tokens
        name = tokenName;                                           // Set the name for display purposes
        symbol = tokenSymbol;                                       // Set the symbol for display purposes
    }

    /**
    * transfer basic function
    */
    function _transfer(address _from, address _to, uint _value) internal {
        // TODO: Make sure, that no token can be sent to the address 0x0
        
        // TODO: Check if the sender has actually enough tokens
        
        // TODO: Check for overflows
        
        // TODO: Update the transaction in the balanceOf map

        //emit transfer-event to meet the ERC20 specification
        emit Transfer(_from, _to, _value);
    }

    /**
    * send value from sender to b
    */
    function transfer(address _to, uint256 _value) public returns (bool success) {
        _transfer(msg.sender, _to, _value);
        return true;
    }

    /**
    * send from address a to address b
    */
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        //TODO: Check if the sender is actually allowed to send the amount of tokens

        //TODO: update the allowance in the mapping

        _transfer(_from, _to, _value);
        return true;
    }

    /**
    * give someone the rights to spend tokens on behalf of some other address
    */
    function approve(address _spender, uint256 _value) public returns (bool success) {
        //TODO: update the allowance
        
        //emit approval event
        emit Approval(msg.sender, _spender, _value);
        return true;
    }


    /**
    * destroy tokens
    */
    function burn(uint256 _value) public returns (bool success) {
        //TODO: check if the sender actually has the amount of tokens he wants to destroy
        
        //TODO: update the balance of the sender
        
        //TODO: update the total supply of our currency
        
        return true;
    }

}
